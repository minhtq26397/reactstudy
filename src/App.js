import logo from './logo.svg';
import './App.css';
import Counter from "./component/Counter/counter";
import SelectCheck from "./component/ShowMeChecked/SelectCheck";
import ShowMeIfYouCan from "./component/ShowMeIfYouCan";
import React from "react";

function App() {
  // function set counter

  return (
    <div className="App">
      {/*<Counter/>*/}
      {/*<SelectCheck/>*/}
      <ShowMeIfYouCan/>
    </div>
  );
}

export default App;

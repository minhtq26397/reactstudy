import React from "react";
import Timer from "./Timer";

export default class Counter extends React.Component{
    constructor(props) {
        super(props);
        this.state = { number: 1}
        this.setCounter=this.setCounter.bind(this)
    }

    setCounter(){
        this.setState({
            number: this.state.number + 1
        });
    }
    render() {
        return(
            <div>
                <Timer num={this.state.number}/>
                <button onClick={this.setCounter}>click me</button>
            </div>
        )
    }
}

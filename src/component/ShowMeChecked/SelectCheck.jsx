import React from 'react'
import WhoisClicked from "./WhoisClicked";

export default class SelectCheck extends React.Component {
    constructor() {
        super();
        this.state = {clicked: null}

    }

    dungClick=()=> {
        this.setState({clicked: 'Dung'})
        this.setState({
            member: this.props.person
        })
    }

    minhClick=()=> {
        this.setState({clicked: 'Minh'})
    }

    huyenClick=()=> {
        this.setState({clicked: 'Huyen'})
    }

    render() {
        return (<div>
            <WhoisClicked person={this.state.clicked}/>
            <button onClick={this.dungClick}>Dung</button>
            <button onClick={this.minhClick}>Minh</button>
            <button onClick={this.huyenClick}>Huyen</button>
        </div>)
    }
}

import React from 'react'

export default class WhoisClicked extends React.Component {
    constructor(props) {
        super(props);
        this.state = {person: null}
    }

    render() {
        return (
            <div style={{height: '20px', border: "solid red"}}>
                <span id = "member">
                    {this.state.person}
                </span>
            </div>
        )
    }

    componentDidMount() {
        console.log('mount call')
        // fixme
        this.setState({
            person: 'I dont know'
        })
    }

    componentDidUpdate() {
        console.log('update call!')
        this.setState({
            person: 'I dont know'
        })
    }
}

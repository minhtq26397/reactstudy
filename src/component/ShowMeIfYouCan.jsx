import React from 'react'

export default class ShowMeIfYouCan extends React.Component {
    constructor() {
        super();
        this.state = {catcher: 'I am chasing you!'}
        this.showYouCallBack = this.showYouCallBack.bind(this);
        this.showYouPromise = this.showYouPromise.bind(this);
        this.catchYou = this.catchYou.bind(this);
    }

    showYouCallBack(callBack) {
        setTimeout(() => {
            let string = 'Now I See YOU!'
            callBack(string)
        }, 500)
    }

    showYouPromise() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                let string = 'Now I See YOU!'
                resolve(string)
            }, 500)
        })
    }
    async showYouAsync(){
        return 'Now I see You'
    }

    async catchYou() {

        // by callback
        // this.showYouCallBack(message=>{
        //     this.setState({catcher:message})
        // })
        //by promise
        // this.showYouPromise().then(message=>{
        //     this.setState({catcher:message})
        // })
        // by async
        // let message = this.showYouPromise()
        let message = await this.showYouAsync()
        this.setState({catcher: message})
    }

    render() {
        return (<div>
            <span>{this.state.catcher}</span>
            <button onClick={this.catchYou}>I catch you</button>
        </div>)
    }
}
